package com.article.restController;

import java.util.Collection;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.article.exception.DataNotFoundException;
import com.article.model.ArticleCategorie;
import com.article.service.ArticleCategorieService;

/**
 * @author a.sallemi
 *
 */
@RestController
@RequestMapping("/api/categories")
public class ArticleCategorieController {

	@Autowired
	ArticleCategorieService articleCategorieService;

	/***
	 * Method to fetch all articles categories from db
	 * 
	 * @return
	 */
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public Collection<ArticleCategorie> getAllArticles() {
		return this.articleCategorieService.getAllCategories();
	}

	/***
	 * Method to fetch article categorie by id
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@GetMapping("/{id}")
	public ResponseEntity<ArticleCategorie> getById(@PathVariable Long id) throws Exception {
		ArticleCategorie articleCategorie = this.articleCategorieService.findCategorieById(id)
				.orElseThrow(() -> new DataNotFoundException("article",id));

		return ResponseEntity.ok().body(articleCategorie);
	}

	/***
	 * Method to update article categorie by id
	 * 
	 * @param id
	 * @param articleCategorieDetails
	 * @return
	 * @throws Exception
	 */
	@PutMapping("/{id}")
	public ResponseEntity<ArticleCategorie> update(@PathVariable long id,
			@RequestBody ArticleCategorie articleCategorieDetails) throws Exception {

		ArticleCategorie articleCategorie = this.articleCategorieService.findCategorieById(id)
				.orElseThrow(() -> new Exception("Article Categorie Not Found for id ::" + id));

		final ArticleCategorie updatedArticle = this.articleCategorieService.updateCategorie(articleCategorie,
				articleCategorieDetails);

		return ResponseEntity.ok().body(updatedArticle);

	}

	/***
	 * Method to create new article categorie(s)
	 * 
	 * @param article Categories
	 * @return
	 */
	@PostMapping
	public ResponseEntity<List<ArticleCategorie>> createEmployee(
			@RequestBody @Valid List<ArticleCategorie> articleCategories) {
		final List<ArticleCategorie> createdArticlesCategories = this.articleCategorieService
				.createArticleCategorie(articleCategories);
		return ResponseEntity.status(HttpStatus.CREATED).body(createdArticlesCategories);
	}

}
