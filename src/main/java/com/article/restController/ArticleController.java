package com.article.restController;

import java.util.Collection;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.article.exception.DataNotFoundException;
import com.article.model.Article;
import com.article.service.ArticleService;

/**
 * @author a.sallemi
 *
 */
@RestController
@RequestMapping("/api/articles")
public class ArticleController {

	@Autowired
	ArticleService articleService;

	/***
	 * Method to fetch all articles from db
	 * 
	 * @return
	 */
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public Collection<Article> getAllArticles() {
		return this.articleService.getAllArticles();
	}

	/***
	 * Method to fetch article by id
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@GetMapping("/{id}")
	public ResponseEntity<Article> getById(@PathVariable Long id) throws Exception {
		Article article = this.articleService.findArticleById(id)
				.orElseThrow(() -> new DataNotFoundException("article categorie",id));

		return ResponseEntity.ok().body(article);
	}

	/***
	 * Method to update article by id
	 * 
	 * @param id
	 * @param articleDetails
	 * @return
	 * @throws Exception
	 */
	@PutMapping("/{id}")
	public ResponseEntity<Article> update(@PathVariable long id, @RequestBody Article articleDetails) throws Exception {

		Article article = this.articleService.findArticleById(id)
				.orElseThrow(() -> new Exception("Article Not Found for id ::" + id));

		final Article updatedArticle = this.articleService.updateArticle(article, articleDetails);

		return ResponseEntity.ok().body(updatedArticle);

	}
	
	
	/***
	 * Method to create new article(s)
	 * @param article
	 * @return
	 */
	@PostMapping
	public ResponseEntity<List<Article>> createEmployee(@RequestBody @Valid  List<Article> articles) {
		articles.forEach(article -> System.out.println(article));
		final List<Article> createdArticles = this.articleService.createArticle(articles);
		return ResponseEntity.status(HttpStatus.CREATED).body(createdArticles);
	}

}
