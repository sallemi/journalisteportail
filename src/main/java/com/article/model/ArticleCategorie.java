package com.article.model;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "article_categorie")
public class ArticleCategorie {

	@Transient
	public static final String SEQUENCE_NAME = "articleCategorie_sequence";

	@Override
	public String toString() {
		return "ArticleCategorie [id=" + id + ", categorieName=" + categorieName + ", description=" + description
				+ ", categoriePicture=" + categoriePicture + "]";
	}

	@Id
	private Long id;

	@NotNull(message = "categorie name cannot be null")
	private String categorieName;

	private String description;

	private String categoriePicture;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCategorieName() {
		return categorieName;
	}

	public void setCategorieName(String categorieName) {
		this.categorieName = categorieName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCategoriePicture() {
		return categoriePicture;
	}

	public void setCategoriePicture(String categoriePicture) {
		this.categoriePicture = categoriePicture;
	}

}
