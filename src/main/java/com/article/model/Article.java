package com.article.model;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "article")
public class Article {

	@Transient
	public static final String SEQUENCE_NAME = "article_sequence";

	@Id
	private Long id;   

	@NotNull(message = "title cannot be null")
	private String title;

	@NotNull(message = "content cannot be null")
	private String content;

	@DBRef
	@NotNull(message = "article categorie cannot be null")
	private ArticleCategorie categorie;

	private String coverPicture;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCoverPicture() {
		return coverPicture;
	}

	public void setCoverPicture(String coverPicture) {
		this.coverPicture = coverPicture;
	}

	public ArticleCategorie getCategorie() {
		return categorie;
	}

	public void setCategorie(ArticleCategorie categorie) {
		this.categorie = categorie;
	}

	@Override
	public String toString() {
		return "Article [id=" + id + ", title=" + title + ", content=" + content + ", categorie=" + categorie
				+ ", coverPicture=" + coverPicture + "]";
	}

}
