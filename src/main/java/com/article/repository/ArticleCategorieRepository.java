package com.article.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.article.model.ArticleCategorie;

@Repository
public interface ArticleCategorieRepository extends MongoRepository<ArticleCategorie, Long>{

}
