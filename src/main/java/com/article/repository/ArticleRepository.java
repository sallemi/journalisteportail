package com.article.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.article.model.Article;

@Repository
public interface ArticleRepository extends MongoRepository<Article, Long>{

}
