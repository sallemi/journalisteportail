package com.article.exception;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;

public class ApiError {

	private HttpStatus status;
	private String message;
	private List<String> errors;
	private LocalDateTime date;

	public ApiError(HttpStatus status, String message, List<String> errors, LocalDateTime date) {
		super();
		this.status = status;
		this.message = message;
		this.errors = errors;
		this.date = date;
	}



	public ApiError(HttpStatus status, String message, String error, LocalDateTime date) {
		// TODO Auto-generated constructor stub
		super();
        this.status = status;
        this.message = message;
        errors = Arrays.asList(error);
        this.date = date;
	}



	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}



	public LocalDateTime getDate() {
		return date;
	}



	public void setDate(LocalDateTime date) {
		this.date = date;
	}


}
