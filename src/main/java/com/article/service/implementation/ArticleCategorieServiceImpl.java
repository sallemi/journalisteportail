package com.article.service.implementation;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.article.model.ArticleCategorie;
import com.article.repository.ArticleCategorieRepository;
import com.article.service.ArticleCategorieService;
import com.article.utils.SequenceGeneratorService;

@Service
public class ArticleCategorieServiceImpl implements ArticleCategorieService {

	@Autowired
	ArticleCategorieRepository articleCategorieRepository;

	@Autowired
	SequenceGeneratorService sequenceGeneratorService;

	@Override
	public List<ArticleCategorie> createArticleCategorie(List<ArticleCategorie> ArticleCategories) {
		ArticleCategories.forEach(categorie -> categorie
				.setId(sequenceGeneratorService.generateSequence(ArticleCategorie.SEQUENCE_NAME)));
		return this.articleCategorieRepository.saveAll(ArticleCategories);
	}

	@Override
	public Collection<ArticleCategorie> getAllCategories() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<ArticleCategorie> findCategorieById(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteCategorieById(long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public ArticleCategorie updateCategorie(ArticleCategorie article, ArticleCategorie articleDetails) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteAllCategories() {
		// TODO Auto-generated method stub

	}

}
