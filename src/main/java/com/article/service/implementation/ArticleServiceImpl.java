package com.article.service.implementation;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.article.model.Article;
import com.article.repository.ArticleRepository;
import com.article.service.ArticleService;
import com.article.utils.SequenceGeneratorService;
@Service
public class ArticleServiceImpl implements ArticleService {

	@Autowired
	ArticleRepository articleRepository;
	
	@Autowired
	SequenceGeneratorService sequenceGeneratorService;
	
	@Override
	public List<Article> createArticle(List<Article> articles) {
		articles.forEach(article -> article.setId(sequenceGeneratorService.generateSequence(Article.SEQUENCE_NAME)));
		return this.articleRepository.saveAll(articles);
	}

	@Override
	public Collection<Article> getAllArticles() {
		return articleRepository.findAll();
	}

	@Override
	public Optional<Article> findArticleById(long id) {
		return articleRepository.findById(id);
	}

	@Override
	public void deleteArticleById(long id) {
		articleRepository.deleteById(id);
	}

	@Override
	public Article updateArticle(Article article,Article articleDetails) {
		
		article.setContent(articleDetails.getContent());
		article.setCoverPicture(articleDetails.getCoverPicture());
		article.setTitle(articleDetails.getTitle());
		article.setCategorie(articleDetails.getCategorie());
		
		return articleRepository.save(article);
	}

	@Override
	public void deleteAllArticle() {
		articleRepository.deleteAll();
	}

}
