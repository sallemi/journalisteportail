package com.article.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.article.model.ArticleCategorie;

public interface ArticleCategorieService {

public List<ArticleCategorie> createArticleCategorie(List<ArticleCategorie> article);
	
	public Collection<ArticleCategorie> getAllCategories();
	
	public Optional<ArticleCategorie> findCategorieById(long id);
	
	public void deleteCategorieById(long id);
	
	public ArticleCategorie updateCategorie(ArticleCategorie article,ArticleCategorie articleCategorieDetails);
	
	public void deleteAllCategories();
	
}
