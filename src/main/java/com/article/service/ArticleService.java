package com.article.service;


import java.util.*;


import com.article.model.Article;

public interface ArticleService {

	public List<Article> createArticle(List<Article> article);
	
	public Collection<Article> getAllArticles();
	
	public Optional<Article> findArticleById(long id);
	
	public void deleteArticleById(long id);
	
	public Article updateArticle(Article article,Article articleDetails);
	
	public void deleteAllArticle();
	
	
}
