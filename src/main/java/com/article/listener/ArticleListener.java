package com.article.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.stereotype.Component;

import com.article.exception.DataNotFoundException;
import com.article.model.Article;
import com.article.model.ArticleCategorie;
import com.article.service.ArticleCategorieService;
import com.article.utils.SequenceGeneratorService;

@Component
public class ArticleListener extends AbstractMongoEventListener<Article> {

	@Autowired
	private SequenceGeneratorService sequenceGeneratorService;
	
	@Autowired
    private MongoOperations mongoOperations;
	
//	@Autowired 
//	private ArticleCategorieService articleCategorieService;

	@Override
	public void onBeforeConvert(BeforeConvertEvent<Article> event) {
		
		Article articleSource=event.getSource();
		//generate id 
		if (articleSource.getId() < 1) {
			articleSource.setId(sequenceGeneratorService.generateSequence(Article.SEQUENCE_NAME));
		}
		
		//save categorie article if categorie not exist
        if (articleSource .getCategorie() != null) {
            ArticleCategorie categorie=mongoOperations.findById(articleSource.getCategorie().getId(), ArticleCategorie.class);
            if(categorie==null) {
            	throw new DataNotFoundException("article categorie",articleSource.getCategorie().getId());
            }
        }
	}
	
	
}
