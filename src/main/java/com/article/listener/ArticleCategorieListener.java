package com.article.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;

import com.article.model.ArticleCategorie;
import com.article.utils.SequenceGeneratorService;

public class ArticleCategorieListener extends AbstractMongoEventListener<ArticleCategorie> {

	@Autowired
	private SequenceGeneratorService sequenceGeneratorService;

	@Override
	public void onBeforeConvert(BeforeConvertEvent<ArticleCategorie> event) {

		if (event.getSource().getId() < 1) {
			event.getSource().setId(sequenceGeneratorService.generateSequence(ArticleCategorie.SEQUENCE_NAME));
		}
	}

}
